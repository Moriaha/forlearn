import { Component, OnInit } from '@angular/core';
import { HardCodedsService } from './hard-codeds.service';

@Component({
  selector: 'jce-hard-codeds',
  templateUrl: './hard-codeds.component.html',
    styles: [`
    .HardCodeds li {cursor: default; }
    .HardCodeds li:hover { background: #ecf0f1; }
    .list-group-item.active,
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
  `]
})
export class HardCodedsComponent implements OnInit {

hards;
currentHard;

select(hard){
this.currentHard=hard;
}
  constructor(private _hardService:HardCodedsService) { }

  ngOnInit() {
    this.hards=this._hardService.getHards();
  }

}