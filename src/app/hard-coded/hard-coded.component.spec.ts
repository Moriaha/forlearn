/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HardCodedComponent } from './hard-coded.component';

describe('HardCodedComponent', () => {
  let component: HardCodedComponent;
  let fixture: ComponentFixture<HardCodedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HardCodedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HardCodedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
