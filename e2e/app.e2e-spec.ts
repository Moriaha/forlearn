import { HomeTestPage } from './app.po';

describe('home-test App', function() {
  let page: HomeTestPage;

  beforeEach(() => {
    page = new HomeTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
